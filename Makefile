# Makefile
SRC_DIR=./src
BUILD_DIR=./build
CODE_DIR=$(SRC_DIR)/code
OUT=$(BUILD_DIR)/main
TEST_DIR=tests
 
test:
	make -C $(TEST_DIR)
 
test_clean:
	make -C $(TEST_DIR) clean
 
main:
	g++ -std=c++0x -I$(CODE_DIR) $(CODE_DIR)/*.cpp $(SRC_DIR)/main.cpp -o $(OUT)
 
all: test main
 
clean: test_clean
	rm $(SRC_DIR)/*.o $(CODE_DIR)/*.o $(OUT)