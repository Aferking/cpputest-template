#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H
#include <memory>

/*
CircularBuffer behaviour:

Thread safety implemented with lookahead method. Supports
a single producer thread, and a single consumer thread.

.push() to full buffer => value ignored and false returned.

.pop() from empty buffer => defaultValue returned.

Memory allocation => dynamic
*/

class CircularBuffer {
    
    public:
    explicit CircularBuffer(int size, int defaultValue = 0);
    bool isEmpty();
    bool isFull();
    bool push(int num);
    int pop();
    int getCapacity();
    
    private:
    int nextIndex(int indx);

    std::unique_ptr<int[]> m_buff;
    int m_capacity = 0;
    int m_index = 0;
    int m_outdex = 0;
    int m_defaultValue = 0;
};

#endif