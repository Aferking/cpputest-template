#include "CircularBuffer.h"

CircularBuffer::CircularBuffer(int capacity, int defaultValue) {
    m_buff = std::unique_ptr<int[]>(new int[capacity+1]);
    m_capacity = capacity+1;
    m_defaultValue = defaultValue;
}

bool CircularBuffer::isEmpty() {
    return m_index == m_outdex;
}

bool CircularBuffer::isFull() {
    return nextIndex(m_index) == m_outdex;
}

bool CircularBuffer::push(int num) {
    if(isFull()) {return false;}
    m_buff[m_index] = num;
    m_index = nextIndex(m_index);
    return true;
}

int CircularBuffer::pop() {
    if(isEmpty()) {return m_defaultValue;}
    int retval = m_buff[m_outdex];
    m_outdex = nextIndex(m_outdex);
    return retval;
}

int CircularBuffer::nextIndex(int indx) {
    indx++;
    if(indx == m_capacity) {
        indx = 0;
    }
    return indx;
}

int CircularBuffer::getCapacity() {
    return m_capacity-1;
}