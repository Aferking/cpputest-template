#include "CppUTest/TestHarness.h"
#include "CircularBuffer.h"


constexpr int capacity = 10;
constexpr int defaultValue = -1;
TEST_GROUP(CircularBuffer)
{
    CircularBuffer buffer = CircularBuffer(capacity, defaultValue);

    void setup() {
    }

    void teardown() {
    }
};

TEST(CircularBuffer, can_be_created_and_destroyed) {
}

TEST(CircularBuffer, is_empty_after_creation) {
    CHECK_TRUE(buffer.isEmpty());
}

TEST(CircularBuffer, is_not_full_after_creation) {
    CHECK_FALSE(buffer.isFull());
}

TEST(CircularBuffer, is_not_empty_after_push) {
    buffer.push(42);
    CHECK_FALSE(buffer.isEmpty());
}

TEST(CircularBuffer, pop_equals_push_for_one_item) {
    buffer.push(42);
    LONGS_EQUAL(42, buffer.pop());
}

TEST(CircularBuffer, pop_push_is_fifo) {
    buffer.push(41);
    buffer.push(42);
    buffer.push(43);
    LONGS_EQUAL(41, buffer.pop());
    LONGS_EQUAL(42, buffer.pop());
    LONGS_EQUAL(43, buffer.pop());
}

TEST(CircularBuffer, report_capacity) {
    LONGS_EQUAL(capacity, buffer.getCapacity());
}

TEST(CircularBuffer, capacity_is_adjustable) {
    buffer = CircularBuffer(capacity+2, defaultValue);
    LONGS_EQUAL(capacity+2, buffer.getCapacity());
}

static void fill_it_up(CircularBuffer &buffer) {
    for(int k=0; k<buffer.getCapacity(); k++) {
        buffer.push(k);
    }
}

TEST(CircularBuffer, fill_to_capacity) {
    fill_it_up(buffer);
    CHECK_TRUE(buffer.isFull());
}

TEST(CircularBuffer, is_not_full_after_get_from_full) {
    fill_it_up(buffer);
    buffer.pop();
    CHECK_FALSE(buffer.isFull());
}

TEST(CircularBuffer, force_a_wraparound) {
    buffer = CircularBuffer(2);
    buffer.push(1);
    buffer.push(2);
    buffer.pop();
    buffer.push(3);
    LONGS_EQUAL(2, buffer.pop());
    LONGS_EQUAL(3, buffer.pop());
    CHECK_TRUE(buffer.isEmpty());
}

TEST(CircularBuffer, full_after_wrapping) {
    buffer = CircularBuffer(2);
    buffer.push(1);
    buffer.push(2);
    buffer.pop();
    buffer.push(3);
    CHECK_TRUE(buffer.isFull());
}

TEST(CircularBuffer, put_to_full_fails){
    buffer = CircularBuffer(1);
    CHECK_TRUE(buffer.push(1));
    CHECK_FALSE(buffer.push(2));
}

TEST(CircularBuffer, get_from_empty_return_default) {
    LONGS_EQUAL(defaultValue, buffer.pop());
}

TEST(CircularBuffer, put_to_full_does_not_dammage_content) {
    buffer = CircularBuffer(1, defaultValue);
    buffer.push(1);
    CHECK_FALSE(buffer.push(2));
    LONGS_EQUAL(1, buffer.pop());
    CHECK_TRUE(buffer.isEmpty());
}

TEST(CircularBuffer, get_from_empty_does_no_harm) {
    buffer = CircularBuffer(1, defaultValue);
    buffer.pop();
    CHECK_TRUE(buffer.isEmpty());
    CHECK_FALSE(buffer.isFull());
    buffer.push(1);
    CHECK_TRUE(buffer.isFull());
    CHECK_FALSE(buffer.isEmpty());
    LONGS_EQUAL(1, buffer.pop());
    CHECK_TRUE(buffer.isEmpty());
    CHECK_FALSE(buffer.isFull());
}
