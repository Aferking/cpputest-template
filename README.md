### CPPUTEST Template
Template for using CppUTest framework.

Contains test code for a circular buffer.


#### Usage:
Copy folder and rename.
Add test code to ./tests folder
Add production code in ./src folder
Run tests to make sure everyting works
Expected results: `OK (16 tests, 16 ran, 28 checks, 0 ignored, 0 filtered out, 0 ms)`
Delete circular buffer code.

### Building

#### Requirements
apt install autoconf
apt install build-essential
apt install libtool

#### Installing cpputest
1. Compile cpputest from [source](https://github.com/cpputest/cpputest). 
    NOTE: You have to build from within the main folder! (follow instructions from cpputest repo).
1. Add `export CPPUTEST_HOME=**` somewhere like .bashrc. Example: `export CPPUTEST_HOME=$HOME/cpputest`
1. Make a directory under CPPUTEST_HOME called `lib`
1. Find libCppUTest.a under CPPUTEST_HOME and move it to the lib folder
1. Find libCppUTestExt.a under CPPUTEST_HOME and move it to the lib folder



#### Building .src/main application
run `make main` from project folder

#### Build and run tests
run `make` from project folder
